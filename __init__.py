from st3m.application import Application, ApplicationContext
import st3m.run
import badgenet
import badgelink
from select import poll, POLLIN
import socket
import audio
import random
import json


class Configuration:
    def __init__(self) -> None:
        self.team = None

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                json_data = f.read()
            data = json.loads(json_data)
        except OSError:
            data = {"team": random.randint(0, 2)}
        if "team" in data and type(data["team"]) == int:
            res.team = data["team"]
        return res

    def save(self, path: str) -> None:
        d = {
            "team": self.team,
        }
        json_data = json.dumps(d)
        with open(path, "w") as f:
            f.write(json_data)
            f.close()


class R0ckPap3erSc1ssor(Application):
    states = {0: 'rock', 1: "scissor", 2: "paper"}

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        badgenet.configure_jack(badgenet.SIDE_LEFT, badgenet.MODE_ENABLE_AUTO)
        badgenet.configure_jack(badgenet.SIDE_RIGHT, badgenet.MODE_ENABLE_AUTO)
        self.addr = "ff02::1%" + badgenet.get_interface().name()
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.bind((self.addr, 2137))
        self.time_since_last_broadcast = 0
        self.poll = poll()
        self.poll.register(self.sock, POLLIN)

        self.bundle_path = app_ctx.bundle_path
        self._filename = "/flash/rock_paper_scissor.json"
        self._config = Configuration.load(self._filename)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # ctx.rgb(255, 192, 203).move_to(0, 0)
        # ctx.text_align = ctx.CENTER
        # ctx.text_baseline = ctx.MIDDLE
        # ctx.font_size = 99
        # ctx.font = ctx.get_font_name(5)
        # ctx.text(str(R0ckPap3erSc1ssor.states[self._config.team]))
        team_img = f"{self.bundle_path}/img/{str(R0ckPap3erSc1ssor.states[self._config.team])}.png"
        ctx.image(team_img, -90, -90, 190, 190)

    def think(self, ins: InputState, delta_ms: int) -> None:
        if not badgelink.left.active() and audio.headphones_are_connected():
            badgelink.left.enable()
        if not badgelink.right.active() and audio.line_in_is_connected():
            badgelink.right.enable()
        self.time_since_last_broadcast += delta_ms
        if self.time_since_last_broadcast > 500:
            self.time_since_last_broadcast -= 500
            self.sock.sendto(str(self._config.team), (self.addr, 2137))

        for s, _ in self.poll.poll(1):
            data, addr = s.recvfrom(1024)
            result = (self._config.team - int(data)) % 3
            if result == 0:
                # draw
                pass
            if result == 1:
                # you lost
                self._config.team = int(data)
                self._config.save(self._filename)
            if result == 2:
                # you won
                pass

    def on_exit(self) -> None:
        self._config.save(self._filename)


if __name__ == "__main__":
    st3m.run.run_responder(R0ckPap3erSc1ssor(ApplicationContext()))
