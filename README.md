# Rock, paper, scissor

## install
* mount flash drive and copy all files into /sys/apps/rock_paper_scissor/

## how to play
* on the first start you are randomly assigned to a team (rock, paper, scissor).
* your team is displayed
* if you see someone from the opposing team get your audio cable ready.
* connect the cable from the left output into the right input and see what happens
